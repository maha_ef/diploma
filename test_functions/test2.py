from numpy import sin, cos, pi


def boundary_func_1(t):
    return sin(t)


def boundary_func_3(t):
    return 3*sin(3*t)


def g_func_1(x):
    return -3*sin(x)


def g_func_3(x):
    return 27*sin(3*x)
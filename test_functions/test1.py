from numpy import sin, cos, pi

def boundary_func(t, m, n):
    return n * sin(m * t)


def g_func(x, m, n):
    return -4*m*sin(n*x)
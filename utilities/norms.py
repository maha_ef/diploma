def l2_norm(target, step):
    summ = 0
    for elem in target:
        summ += elem**2
    norm = summ**(1/2)
    return norm * step
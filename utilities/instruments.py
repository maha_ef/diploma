def cur_cord(idx, step):
    return idx * step


def input_values():
    promt = 'С какой задачей будете работать? \nНажмите 0: M = 5\nНажмите 1: M = 10\nНажмите 2: M = 20\nНажмите 3: M = 50\nНажмите 4: M = 100\nНажмите 5: M = 200\nНажмите 6: M = 300\nНажмите 7: M = 600\nНажмите 8: Введите свое значение M\nВведите значение '
    
    option = int(input(promt))
    
    if option == 0:
        M = 5
    elif option == 1:
        M = 10
    elif option == 2:
        M = 20
    elif option == 3:
        M = 50
    elif option == 4:
        M = 100
    elif option == 5:
        M = 200
    elif option == 6:
        M = 300
    elif option == 7:
        M = 600
    elif option == 8:
        promt = 'M = '
        M = int(input(promt))
        
    return M

def regularization(h):
    promt = 'Какой вариант задачи будет решаться?\nНажмите 0: h=tau\nНажмите 1: h=2*tau\nВведите значение '
    decision = int(input(promt))
    if decision == 0:
        return decision, h
    else:
        return decision, h/2
    
    
def noising():
    promt = 'Необходимо ли зашумить целевой вектор\nНажмите 0: нет\nНажмите 1: да\nВведите значение '
    decision = int(input(promt))
    return decision
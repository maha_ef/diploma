import numpy as np
import math

def max_of_vectors_deviation(first_vector, second_vector):
    tmp_vector = abs(first_vector - second_vector)
    return np.amax(tmp_vector)


def l2_norm_deviation(v1, v2, step):
    sum = 0
    for i in range(len(v1)):
        sum += (v1[i] - v2[i]) ** 2
    
    sum = sum * step
    sum = np.sqrt(sum)
    return sum
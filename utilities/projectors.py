import numpy as np

def final_position_projector(g_vector, length):
    assert length % 2 == 0, 'vectors of final position and final speed have different sizes'
    return g_vector[0 : length // 2]
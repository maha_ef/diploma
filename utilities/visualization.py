import matplotlib.pyplot as plt

def draw_comparison(span, vect1, vect2):
    plt.plot(span, vect1, ls='-.', c='r', label='vect1')
    plt.plot(span, vect2, ls='--', c='g', label='vect2')
    plt.legend(loc='lower left')
    plt.xlabel('span_name')
    plt.show()
    
    
def draw_g_comparison(x_span, g, g_star, q, name):
    plt.plot(x_span, g, ls='-.', c='cyan', label='g')
    plt.plot(x_span, g_star, ls='--', c='k', label='$g^*$')
    plt.legend(loc='upper left')
    plt.xlabel('$x$')
    plt.title(f'$g$ and $g^*$ comparison, noise={q}%')
    plt.savefig(name)
    plt.show()
   
    
def draw_u_comparison(t_span, u, u_star, name, q, left=True, noised=False):
    if left:
        plt.plot(t_span, u_star, ls='-', c='black', label='$u^*$')
        if noised:
            plt.plot(t_span, u, ls=':', c='cyan', label='$u$')
            plt.title(f'$u$ and $u^*$ comparison, noise={q}%')
        else:
            plt.plot(t_span, u, ls='-.', c='cyan', label='$u$')
            plt.title(f'$u$ and $u^*$ comparison, noise={q}%')
    else:
        plt.plot(t_span, u_star, ls='--', c='g', label='$u_2^*$')
        if noised:
            plt.plot(t_span, u, ls='-.', c='r', label='$u_2^{noised}$')
            plt.title('$u_2^{noised}$ and $u_2^*$ comparison')
        else:
            plt.plot(t_span, u, ls='-.', c='r', label='$u_2$')
            plt.title('$u_2$ and $u_2^*$ comparison')
    plt.legend(loc='upper right')
    plt.xlabel('$t$')
    plt.savefig(name)
    plt.show()
from matrices_functions.task_solvers import *

def only_position(M, N, h, tau):
    calc_mat   = np.zeros((M-1, M-1), float)
    
    zero_pos   = np.zeros(M-1, float)
    zero_left  = np.zeros(N-1, float)
    zero_right = np.zeros(N-1, float)
    
    zero_initial_pos   = np.zeros(M-1, float)
    zero_initial_speed = np.zeros(M-1, float)
    
    for i in range(M-1):
        test = np.zeros(M-1, float)
        test[i] = 1
        borders = conj_solver(zero_pos, test, zero_left, zero_right, M, N, h, tau)
        
        calc_mat[:, i] = direct_solver(zero_initial_pos, zero_initial_speed, borders[0], borders[1], M, N, h, tau)[0]
        
    return calc_mat


def only_speed(M, N, h, tau):
    calc_mat = np.zeros((M-1, M-1), float)
    
    zero_speed = np.zeros(M-1, float)
    zero_left  = np.zeros(N-1, float)
    zero_right = np.zeros(N-1, float)
    
    zero_initial_pos   = np.zeros(M-1, float)
    zero_initial_speed = np.zeros(M-1, float)
    
    for i in range(M-1):
        test = np.zeros(M-1, float)
        test[i] = 1
        borders = conj_solver(test, zero_speed, zero_left, zero_right, M, N, h, tau)
        
        calc_mat[:, i] = direct_solver(zero_initial_pos, zero_initial_speed, borders[0], borders[1], M, N, h, tau)[1]
        
    return calc_mat
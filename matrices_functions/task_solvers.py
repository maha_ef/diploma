import numpy as np

def direct_solver(position, speed, left, right, M, N, h, tau):
    calc_mat = np.zeros((N+1, M+1), float)
    
    calc_mat[0, 1:-1] = position
    calc_mat[1, 1:-1] = calc_mat[0, 1:-1] + speed*tau
    
    calc_mat[1:N,  0] = left
    calc_mat[1:N, -1] = right
    
    coef = (tau / h) ** 2
    
    for t in range(1, N):
        for x in range(1, M):
            calc_mat[t+1, x] = 2 * calc_mat[t, x] - calc_mat[t-1, x] + coef * \
                (calc_mat[t, x-1] - 2 * calc_mat[t, x] + calc_mat[t, x+1])
              
    return calc_mat[-1, 1:-1], (calc_mat[-1, 1:-1] - calc_mat[-2, 1:-1])/tau


def conj_solver(position, speed, left, right, M, N, h, tau):
    calc_mat = np.zeros((N+1, M+1), float)
    
    calc_mat[-1, 1:-1] = position
    calc_mat[-2, 1:-1] = calc_mat[-1, 1:-1] - speed*tau
    
    calc_mat[1:N,  0] = left
    calc_mat[1:N, -1] = right

    coef = (tau / h) ** 2
    
    for t in range(N-1, 0, -1):
        for x in range(1, M):
            calc_mat[t-1, x] = 2 * calc_mat[t, x] - calc_mat[t+1, x] + coef * \
                (calc_mat[t, x+1] - 2 * calc_mat[t, x] + calc_mat[t, x-1])
            
    return (calc_mat[1:-1, 1]-calc_mat[1:-1, 0])/h, (calc_mat[1:-1, -2]-calc_mat[1:-1, -1])/h
# Problem of approaching linear manifold for wave equation
## Description of the problem and how it is solved
### Calculating operator's matrix
We have orthonormal basis of *N+1*-dimensional space as input. After acting on it with projection operator, we consistently solve conjugate task for every vector, results of conjugate task are inputs for direct task. After that we act on them with projection operator again. Final vectors are saved in matrix of operator __BAA\*B\*__.
### Calculating vector of accurate analytical solution
With the help of article of Ilyin-Moiseev *"Optimization of boundary controls for string vibrations"* analytically calculate function _**u\***_ values on borders of line segment **X**. 
### Analytical calculation of the target vector
## Files location
./calculation_functions/ - folder with functions for realization method 'cross'  
./debug_functions/ - folder with functions for checking if method is correct  
./test_functions/ - folder with functions for testing program  
./utils/ - folder with functions that are often used  
./black_list.ipynb - temporary main
## Literature  
1. ["Optimization of boundary controls for string vibrations" Ilyin-Moiseev](http://www.mathnet.ru/links/ff33c3945381d5d507beab5d02d36587/rm1678.pdf)


# Задача наведения на линейное многообразие для волнового уравнения
## Описание принципа работы
### Нахождение матрицы оператора
На вход подается ортонормированный базис *N+1*-мерного пространства. Действуем на него оператором проектирования, решаем последовательно сопряженную задачу для каждого из векторов, результаты сопряженной задачи подаются на вход прямой задаче, после чего к ним опять применяется оператор проектирования. После всех воздействий итоговые векторы сохраняются в матрицу оператора __BAA\*B\*__. 
### Поиск вектора точного аналитического решения
С помощью статьи Ильина-Моисеева *"Оптимизация граничных управлений колебаниями струны"* аналитически высчитываем значения функции _**u\***_ на концах отрезка **X**. 
### Аналитическое вычисление вектора цели
Применяя метод 'крест' для задачи с нулевыми начальными позицией и скоростью и граничными условиями, равными значениям функции _**u\***_ на концах отрезка **X**, находим вектор, соответсвующий значениям позиции и скорости искомой функции в конечный момент времени. Действуем оператором проектирования на найденный вектор и получаем вектор-цель _**g\***_.
### Решение СЛАУ
С помощью метода *numpy.linalg.solve()* из пакета numpy решаем СЛАУ с матрицей оператора __BAA\*B\*__ в левой части матричного уравнения и вектором-целью _**g\***_ в правой. Полученный ответ будем хранить в векторе _**v**_. 
### Поиск программного решения
Применяя метод 'крест' для задачи с нулевыми граничными условиями, нулевым финальным состоянием и финальной скоростью, равной *-v*, решаем сопряженную задачу и находим векторы *u_tilde* на концах отрезка **X**. 
### Программное вычисление вектора цели
Применяя метод 'крест' для задачи с нулевыми начальными позицией и скоростью и граничными условиями, равными значениям функции *u_tilde* на концах отрезка **X**, находим вектор, соответсвующий значениям позиции и скорости искомой функции в конечный момент времени. Действуем оператором проектирования на найденный вектор и получаем вектор-цель *g_tilde*.
## Расположение файлов
./calculation_functions/ - папка с функциями реализации шаблонного метода 'крест'  
./debug_functions/ - папка с функциями для проверки корректности метода  
./test_functions/ - папка с функциями для тестирования программы  
./utils/ - папка с часто используемыми функциями  
./black_list.ipynb - временный главный файл  



## Литература
1. ["Оптимизация граничных управлений колебаниями струны" Ильин-Моисеев](http://www.mathnet.ru/links/ff33c3945381d5d507beab5d02d36587/rm1678.pdf)
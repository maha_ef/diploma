import math

def mu(t, m, n, psi_x):
    ret_val = psi_x(0, t, m, n)
    return ret_val if ret_val > 0 else 0


def nu(right_border, t, m, n, psi_x):
    ret_val = -psi_x(right_border, t, m, n)
    return ret_val if ret_val > 0 else 0


def adder(x, t, m, n, right_border, n_limit, psi_x):
    first_addend, third_addend = .0, .0
    for k in range(n_limit + 1):
        first_addend += mu(t - x - 2 * k * right_border, m, n, psi_x)
        third_addend += nu(right_border, t + x - right_border * (2 * k + 1), m, n, psi_x)
        
    second_addend, fourth_addend = .0, .0
    for k in range(1, n_limit + 2):
        second_addend += mu(t + x - 2 * k * right_border, m, n, psi_x)
        fourth_addend += nu(right_border, t - x - right_border * (2 * k - 1), m, n, psi_x)
        
    return first_addend - second_addend + third_addend - fourth_addend